import 'dart:convert';

import 'package:flutter/material.dart';
import '../Responses/SourceResponse.dart';
import 'package:http/http.dart' as http;

class SourcesProvider with ChangeNotifier {
  List<SourceResponse> _sources = [] ;

  List<SourceResponse> get getSources {
    return [..._sources];
  }

  Future<void> getAllResources() async {
    String url =
        "https://newsapi.org/v2/sources?apiKey=37c75c3968394a6abf16550e1aa8445f";

  //  try {
      final response = await http.get(url);
      final loadedDAta = json.decode(response.body) as Map<String ,dynamic>;

      if (loadedDAta["status"] == "ok") {
      loadedDAta["sources"].forEach((element){
        SourceResponse sourceResponse = new SourceResponse(
            name: element["name"],
            id: element["id"],
            description: element["description"],
            url: element["url"],
            category: element["category"],
            country: element["country"],
            language: element["language"]);
        _sources.add(sourceResponse) ;
       // print(sourceResponse.name);

      }) ;

      }

     // print(_sources[0].description);
//    } catch (error) {
//      throw error;
//    }
  }
}
