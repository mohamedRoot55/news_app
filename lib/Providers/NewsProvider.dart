import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:news_app/models/Article.dart';
import 'package:news_app/models/Source.dart';
import '../Responses/News_Response.dart';
import 'package:http/http.dart' as http;

class NewsProviders with ChangeNotifier {
  NewsResponse _head_lines_data;
  NewsResponse _every_thing_data ;

  NewsResponse get getHeadLinesNewsResponse {
  //  print(_data.articles[0].description);
    return _head_lines_data;
  }

  NewsResponse get getEveryThingResponse{
    return _every_thing_data ;
  }
  final String api_key = "37c75c3968394a6abf16550e1aa8445f" ;

  Future<void> getTopHeadLinesArticles(String country, String category) async {
    String url;

    if (country == null && category == null) {
      url =
          "https://newsapi.org/v2/top-headlines?country=us&apiKey=$api_key";
    } else if (country != null) {
      url =
          "https://newsapi.org/v2/top-headlines?$country=us&apiKey=$api_key";
    } else if (country != null && category != null) {
      url =
          "https://newsapi.org/v2/top-headlines?$country=us&category=$category&apiKey=$api_key";
    }

    try {
      final response = await http.get(url);
      final loadedData = json.decode(response.body);
      List<Article> articles = [];
      int totalResults = 0;
      String status = '';
      if (loadedData["status"] == 'ok') {
        totalResults = loadedData["totalResults"];
        status = loadedData["status"];

        loadedData["articles"].forEach((element) {
          Article article = new Article(
              title: element["title"],
              content: element["content"],
              publishedAt: element["publishedAt"],
              urlToImage: element["urlToImage"],
              url: element["url"],
              description: element["description"],
              author: element["author"],
              source: element["source"].forEach((string, source) {
                return Source(id: element["id"], name: element["name"]);
              }));

          articles.add(article);
        });

        NewsResponse newsResponse = NewsResponse(
            articles: articles, totalResults: totalResults, status: status);
        _head_lines_data = newsResponse;

        notifyListeners();
      } else {
        _head_lines_data = NewsResponse(status: "failed", totalResults: 0, articles: []);
      }

      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> getEveryThing(String searchWord) async{
    String url  ;
    if(searchWord != null ){
      url = "https://newsapi.org/v2/everything?q=$searchWord&apiKey=$api_key" ;
    }else {
      url = "https://newsapi.org/v2/everything?q=general&apiKey=$api_key" ;
    }


    try {
      final response = await http.get(url);
      final loadedData = json.decode(response.body);
      List<Article> articles = [];
      int totalResults = 0;
      String status = '';
      if (loadedData["status"] == 'ok') {
        totalResults = loadedData["totalResults"];
        status = loadedData["status"];

        loadedData["articles"].forEach((element) {
          Article article = new Article(
              title: element["title"],
              content: element["content"],
              publishedAt: element["publishedAt"],
              urlToImage: element["urlToImage"],
              url: element["url"],
              description: element["description"],
              author: element["author"],
              source: element["source"].forEach((string, source) {
                return Source(id: element["id"], name: element["name"]);
              }));

          articles.add(article);
        });

        NewsResponse newsResponse = NewsResponse(
            articles: articles, totalResults: totalResults, status: status);
        _every_thing_data = newsResponse;

        notifyListeners();
      } else {
        _every_thing_data = NewsResponse(status: "failed", totalResults: 0, articles: []);
      }

      notifyListeners();
    } catch (error) {
      throw error;
    }

  }
}
