import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class News_Container extends StatelessWidget {
  List<Widget> childrens;

  Decoration decoration;

  double heigh;

  bool appbar;

  // constructor
  News_Container({this.appbar, @required this.childrens, this.decoration, this.heigh});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final news_size = screenSize.width * 0.66;
    final news_top = -(screenSize.width * 0.154);
    final news_right = -(screenSize.width * 0.23);
    final appBarTop = news_size / 2 + news_top - IconTheme.of(context).size / 2;
    return Container(
      height: heigh,
      decoration: decoration,
      child: Stack(
        children: <Widget>[

          Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                if (appbar)
                  Padding(
                    padding: EdgeInsets.only(left: 28, right: 28, top: appBarTop),
                    child:  Row(

                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        InkWell(
                          child: Icon(Icons.arrow_back , color: Colors.grey,),
                          onTap: () {
                            Navigator.of(context).pop();
                          },

                        ) ,
                        Icon(Icons.menu , color: Colors.grey,)
                      ],
                    ),
                  ),



                if(childrens != null) ...childrens
              ],
            ),

        ],
      ),
    );
  }
}
