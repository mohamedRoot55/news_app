import 'package:flutter/material.dart';
import '../Configs/AppColors.dart';

class SearchBar extends StatefulWidget {
 final Function searchFN ;
 final EdgeInsets margin;

  const SearchBar({
    Key key,
    this.margin = const EdgeInsets.symmetric(horizontal: 28),this.searchFN
  }) : super(key: key);



  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18),
      margin: widget.margin,
      decoration: ShapeDecoration(
        shape: StadiumBorder(),
        color: AppColors.grey,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.search),
          SizedBox(width: 13),
          Expanded(
            child: Form(
              key: formKey,
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: "Search sports, poltics, socail etc",
                  hintStyle: TextStyle(
                    fontSize: 14,
                    color: AppColors.black,
                  ),
                  border: InputBorder.none,
                ),onFieldSubmitted: (text){

                  setState(() {
                    widget.searchFN(text) ;
                  });
              },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
