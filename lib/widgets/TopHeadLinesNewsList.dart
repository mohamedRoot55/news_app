import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/Routes/News_Details.dart';
import 'package:news_app/models/Article.dart';

class TopHeadLinesNewsList extends StatelessWidget {
  final List<Article> articles;

  TopHeadLinesNewsList(this.articles);

  @override
  Widget build(BuildContext context) {
    return Container(

      child: Flexible(
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            Article article = articles.elementAt(index);
            return Container(
                height: 100,
                width: MediaQuery.of(context).size.width * .9,
                margin: const EdgeInsets.symmetric(horizontal: 9, vertical: 16),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(15)),
                child: ListTile(
                  onTap: (){
                    Navigator.of(context).pushNamed(NewsDetails.RouteName , arguments: article);
                  },
                  leading: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15),
                    ),
                    child: FadeInImage(
                      placeholder: AssetImage("assets/images/newseball.png"),
                      image: article.urlToImage !=null  ?   NetworkImage(article.urlToImage) :AssetImage("assets/images/newseball.png")  ,
                      fit: BoxFit.cover,
                    ),
                  ),title:  Text(article.title , softWrap: true,maxLines: 3,style: TextStyle(color: Colors.black),),
                )


                );
          },
          itemCount: articles.length,
        ),
      ),
    );
  }
}
