import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/Configs/AppColors.dart';
import '../widgets/News_Container.dart';
import '../widgets/search_bar.dart';
import './Recent_news.dart';
import 'package:provider/provider.dart';
import '../Providers/NewsProvider.dart';
import '../models/Article.dart';
import '../widgets/TopHeadLinesNewsList.dart';

class Home extends StatefulWidget {
  static const cardHeightFraction = 0.7;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool isInit = true;

  bool isSearchOrNot = false;
  bool searchLoaded = false ;
  bool _showTitle;
  bool _showToolbarColor;
  double _cardHeight;
  ScrollController _scrollController;

  List<Article> top_head_lines_articles = [];
  List<Article> allArticles = [];

  List<Article> allArticlesFromSearch = [];

  Future<void> search(String keyword) async {
    if (keyword != null) {
      setState(() {
        isSearchOrNot = true;
        searchLoaded = true ;
      });
      await Provider.of<NewsProviders>(context)
          .getEveryThing(keyword)
          .then((value) {
        setState(() {
          searchLoaded = false ;
          allArticlesFromSearch = Provider.of<NewsProviders>(context)
              .getEveryThingResponse
              .articles;
        });
      });
    } else {
      isSearchOrNot = false;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(onScroll);
  }

  @override
  void initState() {
    _cardHeight = 0;
    _showTitle = false;
    _showToolbarColor = false;
    super.initState();
    _scrollController = ScrollController()..addListener(onScroll);

    Future.delayed(Duration(milliseconds: 2)).then((value) {});
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (isInit) {
      await Provider.of<NewsProviders>(context)
          .getTopHeadLinesArticles(null, null)
          .then((value) {
        setState(() {
          top_head_lines_articles = Provider.of<NewsProviders>(context)
              .getHeadLinesNewsResponse
              .articles;
        });
      });

      await Provider.of<NewsProviders>(context)
          .getEveryThing(null)
          .then((value) {
        allArticles =
            Provider.of<NewsProviders>(context).getEveryThingResponse.articles;
      });
      isInit = false;
    }
  }

  onScroll() {
    if (!_scrollController.hasClients) return;

    final showTitle = _scrollController.offset > _cardHeight - kToolbarHeight;

    final showToolbarColor = _scrollController.offset > kToolbarHeight;

    if (showTitle != _showTitle || showToolbarColor != _showToolbarColor) {
      setState(() {
        _showTitle = showTitle;
        _showToolbarColor = showToolbarColor;
      });
    }
  }

  Widget _buildCard(List<Article> articles ) {
    return News_Container(
      heigh: MediaQuery.of(context).size.height * .9,
      appbar: true,
      decoration: BoxDecoration(color: Colors.white
          //  borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
          ),
      childrens: <Widget>[
        Center(
          child: Text(
            "top head lines",
            style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontStyle: FontStyle.italic,
                fontFamily: "Chomsky"),
          ),
        ),
        SizedBox(height: 30),
        //   SearchBar(),
        SizedBox(
          height: 10,
        ),
        TopHeadLinesNewsList(articles)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    _cardHeight = screenHeight * Home.cardHeightFraction;

    return Scaffold(
      body: Container(
        child: NestedScrollView(

          controller: _scrollController,
          headerSliverBuilder: (_, __) => [
            SliverAppBar(
              backgroundColor: Colors.black,
              elevation: 0,
              expandedHeight: _cardHeight,
               // floating: true,
              pinned: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(30),
                ),
              ),
              flexibleSpace: FlexibleSpaceBar(
                collapseMode: CollapseMode.pin,
                centerTitle: true,
                title: _showTitle
                    ? Text(
                        "Evry thing",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    : null,
                background: _buildCard(top_head_lines_articles != null
                    ? top_head_lines_articles
                    : []),
              ),
            )
          ],
          body: Container(
              margin: EdgeInsets.only(top: 0),
              child: isSearchOrNot
                  ? Recent_news(
                      allArticlesFromSearch != null
                          ? allArticlesFromSearch
                          : [],
                      search)
                  : Recent_news(
                      allArticles != null ? allArticles : [], search)),
        ),
      ),
    );
  }
}
