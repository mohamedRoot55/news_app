import 'package:flutter/material.dart';
import '../Configs/AppColors.dart';
import '../models/Article.dart';
import '../widgets/search_bar.dart';

class Recent_news extends StatelessWidget {
  final List<Article> articles;
 final Function searchFN ;

  Recent_news(this.articles , this.searchFN);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Center(
                child: Text(
                  "Evry thing in below",
                  style: TextStyle(color: AppColors.grey, fontSize: 28),
                ),
              ),
            ),
            SizedBox(height: 30,) ,
            SearchBar(searchFN: searchFN,),
            Flexible(
              child: ListView.builder(
                itemBuilder: (ctx, index) {
                  Article article = articles.elementAt(index);
                  return Container(
                      height: 100,
                      width: MediaQuery.of(context).size.width * .9,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 9, vertical: 16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15)),
                      child: ListTile(
                        leading: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                          child: FadeInImage(
                            placeholder:
                                AssetImage("assets/images/newseball.png"),
                            image: article.urlToImage != null
                                ? NetworkImage(article.urlToImage)
                                : AssetImage("assets/images/newseball.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                        title: Text(
                          article.title,
                          softWrap: true,
                          maxLines: 3,
                          style: TextStyle(color: Colors.black),
                        ),
                      ));
                },
                itemCount: articles.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
