import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/models/Article.dart';
import '../Configs/AppColors.dart';
import 'package:intl/intl.dart';
import './WepView_NewsSource.dart';
import './AllSources.dart';
// This is the type used by the popup menu below.
enum WhyFarther { article_source, our_sources }

class NewsDetails extends StatefulWidget {
  static const String RouteName = '/NewsDetails';

  @override
  _NewsDetailsState createState() => _NewsDetailsState();
}

class _NewsDetailsState extends State<NewsDetails> {
  WhyFarther _selection;

  @override
  Widget build(BuildContext context) {
    final article = ModalRoute.of(context).settings.arguments as Article;
    PopupMenuButton popupMenuButton = PopupMenuButton<WhyFarther>(
      color: Colors.white,
      child: Icon(
        Icons.more_vert,
        color: Colors.white,
      ),
      onSelected: (WhyFarther result) {
        setState(() {
          _selection = result;
        });
        if(result == WhyFarther.article_source){
          Navigator.of(context).pushNamed(New_Source_WepView.routeName , arguments: article.url) ;
        }
        else if(result == WhyFarther.our_sources){
          Navigator.of(context).pushNamed(AllSources.routeName) ;
       }

      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<WhyFarther>>[
        const PopupMenuItem<WhyFarther>(
          value: WhyFarther.article_source,
          child: Text('article source'),
        ),
        const PopupMenuItem<WhyFarther>(
          value: WhyFarther.our_sources,
          child: Text('our sources'),
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Colors.black12,
      body: Container(
        //  height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.symmetric(vertical: 40, horizontal: 28),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  InkWell(
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  popupMenuButton,
//                      Icon(
//                        Icons.menu,
//                        color: Colors.white,
//                      )
                ],
              ),
            ),
            Flexible(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 28),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        article.title,
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Stack(
                        children: <Widget>[
                          ClipRRect(
                              child: article.urlToImage != null
                                  ? Image.network(
                                      article.urlToImage,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.asset("assets/image/newsball.png"),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          Positioned(
                            height: 30,
                            width: MediaQuery.of(context).size.width * .6,
                            bottom: 8,
                            right: 8,
                            child: Container(
                              padding: EdgeInsets.only(top: 5),
                              decoration: ShapeDecoration(
                                shape: StadiumBorder(),
                                color: AppColors.grey,
                              ),
                              child: Text(
                                "${DateFormat.yMMMd().format(DateTime.parse(article.publishedAt))}",
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 60,
                      ),
//                      Text(article.source.name == null
//                          ? ""
//                          : "source : " + article.source.name),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                          article.author != null
                              ? "author : " + article.author
                              : "",
                          style: TextStyle(color: Colors.white)),
                      SizedBox(
                        height: 20,
                      ),
                      Text(article.title,
                          softWrap: true,
                          style: TextStyle(color: Colors.white)),
                      SizedBox(
                        height: 20,
                      ),
                      Text(article.content,
                          softWrap: true,
                          style: TextStyle(color: Colors.white)),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 0),
              child: RaisedButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onPressed: () {},
                child: Text("official wep site"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
