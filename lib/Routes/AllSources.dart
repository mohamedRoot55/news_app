import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/Configs/AppColors.dart';
import '../Providers/SourcesProvider.dart';
import 'package:provider/provider.dart';
import '../Responses/SourceResponse.dart';
import './WepView_NewsSource.dart';

class AllSources extends StatefulWidget {
  static const String routeName = "/AllSources";

  @override
  _AllSourcesState createState() => _AllSourcesState();
}

class _AllSourcesState extends State<AllSources> {
  bool isInit = true;

  List<SourceResponse> sourceResponse = [];

//  @override
//  void initState() {
//    super.initState();
//    Future.delayed(Duration(milliseconds: 2)).then((value) {
//      Provider.of<SourcesProvider>(context).getAllResources().then((value) {
//        sourceResponse = Provider.of<SourcesProvider>(context).getSources;
//      });
//    });
//  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (isInit) {
      Provider.of<SourcesProvider>(context).getAllResources().then((value) {
        setState(() {
          sourceResponse = Provider.of<SourcesProvider>(context).getSources;
        });

      });
      isInit = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.black,
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            SourceResponse response = sourceResponse[index];
            return Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              padding: const EdgeInsets.all(16),
              child: Card(
                elevation: 10,
                child: ListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed(
                        New_Source_WepView.routeName,
                        arguments: response.url);
                  },
                  title: Text(response.name),
                  subtitle: Text(response.description),
                  contentPadding: EdgeInsets.all(10),
                ),
              ),
            );
          },
          itemCount: sourceResponse.length,
        ),
      ),
    );
  }
}
