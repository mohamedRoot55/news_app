import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
class New_Source_WepView extends StatelessWidget {
 static const String routeName = "/New_Source_WepView"  ;

  @override
  Widget build(BuildContext context) {
   final String url = ModalRoute.of(context).settings.arguments as String ;
    return Scaffold(
     body: WebView(initialUrl:url , ),
    ) ;
  }
}
