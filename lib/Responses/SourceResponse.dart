class SourceResponse {
 final String id;
 final String name;
 final String description;
 final String url;
 final String category;
 final String language;
 final String country;

 SourceResponse({this.id, this.name, this.description, this.url, this.category,
  this.language, this.country});

}
