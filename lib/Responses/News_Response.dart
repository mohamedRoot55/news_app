
import '../models/Article.dart';
class NewsResponse {
  final String status;
  final int totalResults;
  final List<Article> articles ;

  NewsResponse({this.status, this.articles, this.totalResults});

}
