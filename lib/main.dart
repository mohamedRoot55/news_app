import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './Providers/NewsProvider.dart';
import './Routes/Home.dart';
import './Routes/News_Details.dart';
import './Routes/WepView_NewsSource.dart';
import './Routes/AllSources.dart';
import './Providers/SourcesProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: NewsProviders(),
        ),
        ChangeNotifierProvider.value(value: SourcesProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: "AnticSlab",
          primarySwatch: Colors.blue,
        ),
        home: Home(),
        routes: {
          NewsDetails.RouteName: (ctx) => NewsDetails(),
          New_Source_WepView.routeName: (ctx) => New_Source_WepView(),
          AllSources.routeName: (ctx) => AllSources(),
        },
      ),
    );
  }
}
