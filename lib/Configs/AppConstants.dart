class Constants {
  static final String TOP_HEADLINE_BASE_URL =
      "https://newsapi.org/v2/top-headlines?country=us&apiKey=37c75c3968394a6abf16550e1aa8445f";

  Map<String, String> languages = {
    "arabic": "ae",
    "english": "us",
  };

  Map<String, String> categories = {
    "entertainment": "entertainment",
    "general": "general",
    "health": "health",
    "science": "science",
    "sports": "sports",
    "technology": "technology"
  };
}
